#!/bin/bash

# mkdir $JENKINS_HOME/canvass_labs_tools
# cd $JENKINS_HOME/canvass_labs_tools
# git clone https://cl_test1@bitbucket.org/cl_test1/dist_test.git
# cd $JENKINS_HOME/canvass_labs_tools/dist_test

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py --user
alias pip3 pip
pip install --user virtualenv
$PATH=$HOME/.local/bin:$PATH
virtualenv ENV
activate ENV/bin/activate
pip install --user --upgrade pip
pip install --user -r requirements.txt
